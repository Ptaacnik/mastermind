import balltypes from "./balltypes.js";

export default class BallBase extends Phaser.GameObjects.Container {
    constructor(data) {
        let {scene, x, y, z = 0} = data;
        let balltype = balltypes[z];
        let ballImage = new Phaser.GameObjects.Image(scene, 0, 0, balltype.image);
        super(scene, x, y, ballImage);
        this.ballColor = balltype.ballColor;
        this.ballImage = ballImage;
        this.type = balltype.type;
        this.depth = balltype.depth;
        this.scene = scene;
        this.scene.add.existing(this);
    }
}
