import boxtypes from "./boxtypes.js";

export default class BoxBase extends Phaser.GameObjects.Container {
    constructor(data) {
        let {scene, x, y, z = 0, text} = data;
        let boxtype = boxtypes[z];
        let boxImage = new Phaser.GameObjects.Image(scene, 0, 0, boxtype.image);
        let textName = new Phaser.GameObjects.Text(scene, boxImage.x - 8, boxImage.y - 18, text, {
            font: "32px Arial",
            color: boxtype.color
        });
        super(scene, x, y, [boxImage, textName]);
        this.boxImage = boxImage;
        this.textName = textName;
        this.color = boxtype.color;
        this.type = boxtype.type;
        this.scene = scene;
        this.depth = 1;
        this.scene.add.existing(this);
    }
}
