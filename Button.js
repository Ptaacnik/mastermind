export default class Button extends Phaser.GameObjects.Container{
    constructor(data) {
        let {scene, x, y, image, onpointerdown} = data;
        let buttonImage = new Phaser.GameObjects.Image(scene, 0, 0, image)
        super(scene, x, y, buttonImage, onpointerdown);
        this.buttonImage = buttonImage;
        this.onpointerdown = onpointerdown;
        this.setSize(this.buttonImage.width, this.buttonImage.height);
        this.setInteractive();
        this.on('pointerdown', () => {
            this.buttonImage.tint = 0xcccccc;
            this.onpointerdown();
        })
        this.on('pointerup', () => {
            this.buttonImage.tint = 0xffffff;
        })
        this.scene.add.existing(this);
    }
}
