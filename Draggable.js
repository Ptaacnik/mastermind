import BallBase from "./BallBase.js";


export default class Draggable extends BallBase {
    constructor(data) {
        let {ondragend} = data;
        super(data);
        this.originalX = this.x;
        this.originalY = this.y;
        this.draggable = true;
        this.beingDragged = false;
        this.ondragend = ondragend;
        this.setSize(this.ballImage.width, this.ballImage.height);
        this.setInteractive();
        this.scene.input.setDraggable(this);
        this.scene.input.on('drag', (pointer, gameObject, dragX, dragY) => {
            if (!this.draggable) return;
            this.beingDragged = true;
            gameObject.x = dragX;
            gameObject.y = dragY;
            gameObject.depth = 2;
        })
        this.scene.input.on('dragend', (pointer, gameObject) => {
            this.beingDragged = false;
            gameObject.depth = 1;
            gameObject.ondragend(pointer, gameObject);
        })
    }

    tileCheck(grid) {
        let position = 0;
        if (this.y > grid.yStart - this.height && this.y < grid.yStart + this.height) {
            if (this.x < grid.xOffset - this.width || this.x > grid.xOffset + 300 + this.width) {
            } else {
                if (this.x >= grid.xOffset -10 - this.width && this.x < grid.xOffset -10 + this.width) {
                    position = 0;
                } else if (this.x >= grid.xOffset + 90 - this.width && this.x < grid.xOffset + 90 + this.width) {
                    position = 1;
                } else if (this.x >= grid.xOffset + 190 - this.width && this.x < grid.xOffset + 190 + this.width) {
                    position = 2;
                } else if (this.x >= grid.xOffset + 290 - this.width && this.x < grid.xOffset + 300 + this.width) {
                    position = 3;
                }
                grid.changeBalls(position, this.type);
            }
        }
    }
}
