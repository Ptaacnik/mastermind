import BallBase from "./BallBase.js";

export default class Base {
    constructor(data) {
        let {scene, columns, rows} = data;
        this.xOffset = 250;
        this.yOffset = 75;
        this.yStart = scene.game.config.height * 0.85;
        this.columns = columns;
        this.rows = rows;
        this.scene = scene;
        this.balls = [];
        this.addBalls(0);
    }

    addBalls(start) {
        for (let index = start; index < this.columns * this.rows; index++) {
            let ball = new BallBase({
                scene: this.scene,
                x: this.xOffset + 100 * (index % this.columns),
                y: this.yStart - this.yOffset * Math.floor(index / this.columns),
                z: 0
            });
            this.balls.push(ball);
        }
    }

    changeBalls(x = 0, y = 0) {
        let ball = new BallBase({
            scene: this.scene,
            x: this.balls[x].x,
            y: this.balls[x].y,
            z: y
        });
        if (this.balls[x].y !== 0){
            this.balls[x].destroy();
        }
        this.balls.splice(x, 1, ball);
    }

    moveUp() {
        let x = 0;
        let y = 0;
        for (let i = 0; i < this.columns * this.rows - this.columns; i++) {
            x = (this.rows * this.columns) - 1 - i;
            y = this.balls[x - this.columns].type;
            this.changeBalls(x, y);
        }
        let x1 = 0;
        for (let j = 0; j < this.columns; j++){
            x1 = this.columns - 1 - j;
            this.changeBalls(x1,0);
        }
    }
}
