import Draggable from './Draggable.js';
import Grid from "./Grid.js";
import Button from "./Button.js";
import ScoreGrid from "./ScoreGrid.js";

export class MainScene extends Phaser.Scene {

    constructor() {
        super('MainScene');
    }


    preload() {
        this.load.image('ptak', 'assets/images/Ptacnik_logo.png');
        this.load.image('background', 'assets/images/background_temp.png');
        this.load.image('empty', 'assets/images/empty_temp.png');
        this.load.image('red', 'assets/images/red_temp.png');
        this.load.image('blue', 'assets/images/blue_temp.png');
        this.load.image('green', 'assets/images/green_temp.png');
        this.load.image('yellow', 'assets/images/yellow_temp.png');
        this.load.image('button', 'assets/images/button.png');
        this.load.image('scoreBox1', 'assets/images/score_box_1.png');
        this.load.image('scoreBox2', 'assets/images/score_box_2.png');
        this.load.image('scoreBox0', 'assets/images/score_box_0.png');
        this.load.image('restartButton', 'assets/images/restartButton.png');
    }


    create() {
        this.score = 10000;
        if(localStorage.getItem('HS') === null){
            this.highScore = 0;
            localStorage.setItem('HS', 0);
        } else {
            this.highScore = localStorage.getItem('HS');
        }
        this.scoreText = this.add.text(606, 36, 'Score: ' + this.score, {font: '30px Arial', fill: '#FFF'});
        this.highScoreText = this.add.text(636, 116, 'High Score: ' + this.highScore, {font: '20px Arial', fill: '#FFF'});
        this.background = this.add.image(this.game.config.width / 2, 400, 'background');

        this.grid = new Grid({
            scene: this,
            columns: 4,
            rows: 10
        });
        this.scoreGrid = new ScoreGrid({
            scene: this,
            rows: this.grid.rows,
            x: this.grid.balls[0].x,
            y: this.grid.balls[0].y
        });

        this.code = [];
        this.randomCode(this.code);
        this.turncounter = 1;

        let y1 = 840;
        this.redBall = new Draggable({
            scene: this,
            x: 250,
            y: y1,
            z: 1,
            ondragend: () => {
                this.redBall.tileCheck(this.grid);
                this.redBall.x = this.redBall.originalX;
                this.redBall.y = this.redBall.originalY;
            }
        });
        this.blueBall = new Draggable({
            scene: this,
            x: 350,
            y: y1,
            z: 2,
            ondragend: () => {
                this.blueBall.tileCheck(this.grid);
                this.blueBall.x = this.blueBall.originalX;
                this.blueBall.y = this.blueBall.originalY;
            }
        });
        this.greenBall = new Draggable({
            scene: this,
            x: 450,
            y: y1,
            z: 3,
            ondragend: () => {
                this.greenBall.tileCheck(this.grid);
                this.greenBall.x = this.greenBall.originalX;
                this.greenBall.y = this.greenBall.originalY;
            }
        });
        this.yellowBall = new Draggable({
            scene: this,
            x: 550,
            y: y1,
            z: 4,
            ondragend: () => {
                this.yellowBall.tileCheck(this.grid);
                this.yellowBall.x = this.yellowBall.originalX;
                this.yellowBall.y = this.yellowBall.originalY;
            }
        });
        this.button = new Button({
            scene: this,
            x: this.grid.balls[3].x + 150,
            y: this.grid.balls[3].y,
            image: 'button',
            onpointerdown: () => {
                this.submit(this, this.score, this.scoreText, this.turncounter);
                this.updateScore(this.score, this.scoreText);

            }
        });
    }

    submit(scene, score, scoreText, turnCounter) {
        let guess = [];
        let x = 0;
        for (let i = 0; i < this.grid.columns; i++) {
            x = this.grid.balls[i].type;
            guess.push(x);
        }
        this.blackCheckCount = this.codeCheckBlack(this.code, guess);
        this.whiteCheckCount = this.codeCheckWhite(this.code, guess);

        if (this.blackCheckCount === this.grid.columns) {
            this.win = true;
            this.alertWinLose(this.win);
            let restartButton = scene.add.image(scene.game.config.width / 2, scene.game.config.height / 2, 'restartButton');
            restartButton.depth = 2;
            restartButton.setInteractive();
            restartButton.on('pointerdown', () => {
                restartButton.tint = '#ffffff';
                scene.scene.restart();
            });
            this.scoreGrid.changeBoxes(0, 2, this.blackCheckCount.toString());
            this.scoreGrid.changeBoxes(1, 1, this.whiteCheckCount.toString());
            if(localStorage.getItem('HS').valueOf() <= this.score) {
                localStorage.setItem('HS', this.score);
            }
        } else {
            if (this.grid.balls[0].type === 0 || this.grid.balls[1].type === 0 || this.grid.balls[2].type === 0 || this.grid.balls[3].type === 0) {
                this.alertMistake();
            } else if (turnCounter.valueOf() >= 10) {
                this.alertWinLose(this.win);
                let restartButton = scene.add.image(scene.game.config.width / 2, scene.game.config.height / 2, 'restartButton');
                restartButton.depth = 2;
                restartButton.setInteractive();
                restartButton.on('pointerdown', () => {
                    restartButton.tint = '#ffffff';
                    scene.scene.restart();
                });
                this.scoreGrid.changeBoxes(0, 2, this.blackCheckCount.toString());
                this.scoreGrid.changeBoxes(1, 1, this.whiteCheckCount.toString());
            } else {
                this.scoreGrid.changeBoxes(0, 2, this.blackCheckCount.toString());
                this.scoreGrid.changeBoxes(1, 1, this.whiteCheckCount.toString());
                this.grid.moveUp();
                this.scoreGrid.moveUp();
                this.turncounter += 1;
                this.updateScore(score, scoreText);
                this.score -= 1000;

            }
        }
    }

    randomCode(code = []) {
        for (let i = 0; i < this.grid.columns; i++) {
            code.push(Math.floor(Math.random() * Math.floor(4)) + 1);
        }
    }

    codeCheckWhite(code = [], guess = []) {
        let finalScore = 0;
        let throwTrue = false;
        let throwAway = 0;
        for (let i = 0; i < this.grid.columns; i++) {
            for (let j = 0; j < guess.length; j++) {
                if (code[i] === guess[j]) {
                    finalScore++;
                    throwTrue = true;
                    throwAway = j;
                    j += guess.length;
                } else {
                    throwTrue = false;
                }
            }
            if (throwTrue) {
                guess.splice(throwAway, 1)
            }
        }
        return finalScore;
    }

    codeCheckBlack(code = [], guess = []) {
        let finalScore = 0;
        for (let i = 0; i < this.grid.columns; i++) {
            if (code[i] === guess[i]) {
                finalScore++;
            }
        }
        return finalScore;
    }

    alertMistake() {
        let rectangle = this.add.rectangle(this.game.config.width / 2, this.game.config.width / 2, this.game.config.width, this.game.config.height + 100, '#ffffff', 0.7);
        rectangle.depth = 2;
        rectangle.setInteractive();
        let rectangle2 = this.add.rectangle(this.game.config.width / 2, this.game.config.width / 2, 200, 100, '#ffffff', 1);
        rectangle2.depth = 2;
        let text = this.add.text(this.game.config.width / 2 - rectangle2.width / 2 + 5, this.game.config.width / 2 - rectangle2.height / 2 + 5, "You need to fill up the entire sequence to place your guess!", {
            font: "20px Arial",
            wordWrap: {width: 200, useAdvancedWrap: true}
        })
        text.depth = 2;
        rectangle.on('pointerdown', () => (rectangle.destroy(), rectangle2.destroy(), text.destroy()));
    }

    alertWinLose(win) {
        let rectangle = this.add.rectangle(this.game.config.width / 2, this.game.config.width / 2, this.game.config.width, this.game.config.height + 100, '#ffffff', 0.7);
        rectangle.depth = 2;
        let rectangle2 = this.add.rectangle(this.game.config.width / 2, this.game.config.width / 2 - 200, 600, 200, '#ffffff', 1);
        rectangle2.depth = 2;
        if (win) {
            let text = this.add.text(this.game.config.width / 2 - rectangle2.width / 2 + 5, this.game.config.width / 2 - rectangle2.height / 2 + 5 - 200 + rectangle2.height / 4, "Congratulations!", {
                font: "80px Arial",
                wordWrap: {width: 600, useAdvancedWrap: true}
            })
            text.depth = 2;
        } else {
            let text = this.add.text(this.game.config.width / 2 - rectangle2.width / 2 + 5, this.game.config.width / 2 - rectangle2.height / 2 + 5 - 200 + rectangle2.height / 4, "Better luck next time!", {
                font: "63px Arial",
                wordWrap: {width: 600, useAdvancedWrap: true}
            })
            text.depth = 2;
        }

        rectangle.setInteractive();
    }

    updateScore(score, scoreText){
        scoreText.setText('Score: ' + score);
    }

}
