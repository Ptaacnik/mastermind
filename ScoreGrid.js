import BoxBase from "./BoxBase.js";

export default class ScoreGrid {
    constructor(data) {
        let {scene, rows, x, y} = data;
        this.xOffset = 200;
        this.yOffset = 75;
        this.columns = 2;
        this.rows = rows;
        this.xStart = x;
        this.yStart = y;
        this.scene = scene;
        this.boxes = [];
        this.addBoxes(0);
    }

    addBoxes(start) {
        for (let index = start; index < this.columns * this.rows; index++) {
            let box = new BoxBase({
                scene: this.scene,
                x: this.xStart - this.xOffset + 100 * (index % this.columns),
                y: this.yStart - this.yOffset * Math.floor(index / this.columns),
                z: 0,
                text: ''
            });
            box.boxImage.alpha = 0.2;
            this.boxes.push(box);
        }
    }

    changeBoxes(x = 0, y = 0, text = '') {
        let box = new BoxBase({
            scene: this.scene,
            x: this.boxes[x].x,
            y: this.boxes[x].y,
            z: y,
            text: text
        });

        if (this.boxes[x].y !== 0){
            this.boxes[x].destroy();
        }
        this.boxes.splice(x, 1, box);
    }

    moveUp() {
        let x = 0;
        let y = 0;
        let text = '';
        for (let i = 0; i < this.columns * this.rows - this.columns; i++) {
            x = (this.rows * this.columns) - 1 - i;
            y = this.boxes[x - this.columns].type;
            text = this.boxes[x - this.columns].textName.text;
            this.changeBoxes(x, y, text);
        }
        let x1 = 0;
        for (let j = 0; j < this.columns; j++){
            x1 = this.columns - 1 - j;
            this.changeBoxes(x1,0);
        }
    }
}
