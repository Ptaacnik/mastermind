export let balltypes = [
    {
        ballColor: 'Empty',
        image: 'empty',
        type: 0,
        depth: 1
    },
    {
        ballColor: 'Red',
        image: 'red',
        type: 1,
        depth: 1
    },
    {
        ballColor: 'Green',
        image: 'green',
        type: 2,
        depth: 1
    },
    {
        ballColor: 'Blue',
        image: 'blue',
        type: 3,
        depth: 1
    },
    {
        ballColor: 'Yellow',
        image: 'yellow',
        type: 4,
        depth: 1
    }
]
export default balltypes;
