export let boxtypes = [
    {
        image: 'scoreBox0',
        color: '#000000',
        type: 0
    },
    {
        image: 'scoreBox1',
        color: '#000000',
        type: 1
    },
    {
        image: 'scoreBox2',
        color: '#ffffff',
        type: 2
    }]
export default boxtypes;
