import {MainScene} from './MainScene.js';

const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 870,
    backgroundColor: '#adbcbc',
    parent: 'MMGAME',
    scene: [MainScene]
};

new Phaser.Game(config);
